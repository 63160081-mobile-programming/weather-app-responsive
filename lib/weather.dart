import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(WeatherApp());
}


class WeatherApp extends StatefulWidget{
  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }

  Widget buildDay1Button() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.sunny,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("22°/27°"),
      ],
    );
  }

  Widget buildDay2Button() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.cloud,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("24°/30°"),
      ],
    );
  }

  Widget buildDay3Button() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.cloudy_snowing,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("26°/32°"),
      ],
    );
  }

  Widget buildDay4Button() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.sunny,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("24°/29°"),
      ],
    );
  }

  Widget buildDay5Button() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.cloud,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("23°/27°"),
      ],
    );
  }

  Widget buildDay6Button() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.cloudy_snowing,
            // color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("25°/29°"),
      ],
    );
  }

  //ListTile
  Widget TomorrowListTile(){
    return ListTile(
      leading: Icon(Icons.sunny),
      title: Text("Tomorrow"),
      subtitle: Text("25°/29°"),
    );
  }

  Widget Day1ListTile(){
    return ListTile(
      leading: Icon(Icons.cloud),
      title: Text("Thursday"),
      subtitle: Text("24°/30°"),
    );
  }

  Widget Day2ListTile(){
    return ListTile(
      leading: Icon(Icons.cloudy_snowing),
      title: Text("Friday"),
      subtitle: Text("26°/32°"),
    );
  }
  Widget Day3ListTile(){
    return ListTile(
      leading: Icon(Icons.sunny),
      title: Text("Saturday"),
      subtitle: Text("24°/29°"),
    );
  }
  Widget Day4ListTile(){
    return ListTile(
      leading: Icon(Icons.cloud),
      title: Text("Sunday"),
      subtitle: Text("23°/27°"),
    );
  }
  Widget Day5ListTile(){
    return ListTile(
      leading: Icon(Icons.cloudy_snowing),
      title: Text("Monday"),
      subtitle: Text("25°/29°"),
    );
  }
  buildAppBarWidget(){
    return AppBar(
      backgroundColor: Colors.black87,
      leading: Icon(Icons.arrow_back),
    );
  }

  Widget buildBodyWidget(){
    return ListView (
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 10
              ),
              //Height constraint at Container widget level
              height: 20,
              child: Icon(
                Icons.sunny,
                size: 100,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(top: 100),
              //Height constraint at Container widget level
              height: 100,
              child: Text(" 23°",style: TextStyle(
                fontSize: 72,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              height: 30,
                child: Text("Chonburi Thailand",
                  style: TextStyle(
                    fontSize: 24,
                  ),
                    textAlign: TextAlign.center,
                  ),
            ),
            Container(
              height: 30,
              child: Text("21°/26°",
                style: TextStyle(
                  fontSize: 24,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child: WeatherActionItems(),
            ),
            Divider(
              color: Colors.black,
            ),
            TomorrowListTile(),
            Day1ListTile(),
            Day2ListTile(),
            Day3ListTile(),
            Day4ListTile(),
            Day5ListTile(),
          ],
        ),
      ],
    );
  }

  Widget WeatherActionItems(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildDay1Button(),
        buildDay2Button(),
        buildDay3Button(),
        buildDay4Button(),
        buildDay5Button(),
        buildDay6Button(),
      ],
    );
  }
}

